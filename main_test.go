package main

import (
	"fmt"
	"testing"
)

func TestSayHello(t *testing.T) {

	mockRead := func() (string, error) { return "John", nil }
	writtenLines := []string{}
	mockWrite := func(a ...interface{}) (int, error) {

		line := fmt.Sprint(a...)
		writtenLines = append(writtenLines, line)
		return 1, nil
	}

	SayHello(mockRead, mockWrite)

	AssertArray(t, writtenLines, []string{
		"What's your name? ",
		"Hello John\n",
	})
}

func AssertArray(t *testing.T, array []string, expected []string) {

	arrayLen := len(array)
	expectedLen := len(expected)
	if arrayLen != expectedLen {
		t.Errorf("array size %d is different from expected size %d", arrayLen, expectedLen)
	}
	for index, expectedLine := range expected {
		line := array[index]
		if line != expectedLine {
			t.Errorf("'%s' array line at index %d is different from expected line '%s'", line, index, expectedLine)
		}
	}
}
