package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	SayHello(readLine, fmt.Print)
}

func SayHello(read func() (string, error), write func(...interface{}) (int, error)) {
	_, _ = write("What's your name? ")
	name, err := read()
	if err != nil {
		log.Fatal(err)
	}
	_, _ = write("Hello ", name, "\n")
}

func readLine() (string, error) {
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		return scanner.Text(), nil
	}
	return "", scanner.Err()
}
